import java.awt.Color;
import java.awt.Graphics;

public class Snake {
	//attributen
	private LinkedList<Cell> ll;
	
	private int gewensteSize;
	private Scherm scherm;
	
	private int xcoord = 60, ycoord = 19;
	
	private boolean naarRechts = false, naarLinks = true, naarBoven = false, naarBeneden = false;
	
	
	//constructors
	
	public Snake(Scherm scherm){
		ll = new LinkedList<Cell>();	
		for(int i = 0; i < 5; i++) {
			ll.prepend(new Cell(xcoord - i, ycoord));
		}
		this.scherm = scherm;
		this.gewensteSize = ll.getSize();
	}

	// getters & setters

	public boolean getNaarRechts() {return naarRechts;}
	public boolean getNaarLinks() {return naarLinks;}
	public boolean getNaarBoven() {return naarBoven;}
	public boolean getNaarBeneden() {return naarBeneden;}
	
	public void setNaarRechts(boolean naarRechts) {
		this.naarRechts = naarRechts;
	}
	public void setNaarLinks(boolean naarLinks) {
		this.naarLinks = naarLinks;
	}
	public void setNaarBoven(boolean naarBoven) {
		this.naarBoven = naarBoven;
	}
	public void setNaarBeneden(boolean naarBeneden) {
		this.naarBeneden = naarBeneden;
	}
	
	public int getSize() {
		return ll.getSize();
	}
	
	public void teken(Graphics g) {
		for(int i = 0; i< ll.getSize(); i++) {
			ll.getElement(i).teken(g, Color.GREEN);
		}
	}
	
	public Cell getCell(int i) {
		return ll.getElement(i);
	}
	
	public void removeLastCell() {
		ll.removeLastNode();
	}
	
	public void eentjeLanger() {
		gewensteSize++;
	}
	
	//per game tick
	public void tick() {
		
		//hieronder de collisions met zichzelf
		for(int i = 1; i < ll.getSize() - 1; i++) {//hij mag de head niet checken, daarom begint i bij 1
			if (xcoord == ll.getElement(i).getX() && ycoord == ll.getElement(i).getY()){
					scherm.stop();
			}
		}
		
		//hieronder collisions met muur
		if(xcoord<0 || ycoord<0||xcoord>79||ycoord>39) {
			scherm.stop();
		}
		
		// hieronder het bewegen van de snake
		
		if(naarRechts) xcoord ++;
		if(naarLinks) xcoord --;
		if(naarBoven) ycoord --;
		if(naarBeneden) ycoord ++;
		
		//toevoegen van cell op volgende plaats
		ll.prepend(new Cell(xcoord, ycoord, 10));
		
		if(gewensteSize < ll.getSize())	this.removeLastCell(); 
		//hierdoor wordt de laatste cell verwijderd
	}
	
}

import java.awt.Color;
import java.awt.Graphics;

/**
 * een Cell is de basis van ieder element, namelijk Ball, Paddle en de Snake.
 * @author robin nollet
 *
 */
public class Cell {

	private double x, y;
	private int breedte, hoogte;
	
	//constructor
	
	public Cell(double x, double y, int tileSize) {//tilesize is de grootte van 1 "cel"
		this.x = x;
		this.y = y;
		breedte = tileSize;
		hoogte = tileSize;
	}
	
	public Cell(double x, double y) {
		this(x, y, 10);
	}
	
	//getters en setters:
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	//methoden
	
	public void teken(Graphics g, Color kleur) {
		g.setColor(Color.BLACK);
		g.fillRect((int) x * breedte,(int) y * hoogte, breedte, hoogte);
		g.setColor(kleur);//door dit 2de vierkant krijg je een '3D-effect'
		g.fillRect((int) x * breedte + 2, (int) y * hoogte +2, breedte-2, hoogte-2);
	}
	
}

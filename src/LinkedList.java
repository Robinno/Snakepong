
public class LinkedList<E> {
	
	private int size;
	private Node<E> head;
	
	public LinkedList() {
		this.size = 0;
		this.head = null;
	}
	
	//getters en setters
	public int getSize() {return size;}
	
	public void prepend(E element) {
		Node<E> temp = new Node<>(element, head);
		head = temp;
		size++;
	}
	
	public E getElement(int i) {
		return this.getNode(i).getElement();
	}
	
	public Node<E> getNode(int i){
		Node<E> cursor = head;
		
		if(i == 0) return cursor;
		
		if(i < size && i > 0) {
			for(int j = 0; j < i; j++) {
				cursor = cursor.getNode();
			}
			return cursor;
		}
		
		return null;
	}
	
	public void removeLastNode() {
		Node<E> VoorlaatsteNode = this.getNode(size - 2);
		VoorlaatsteNode.setNext(null);
		size--;
	}
	
	//hieronder de private klasse Node
	
	private class Node<E> {
		
		//attributen
		
		private E element;
		private Node<E> next;
		
		//constructor
		
		public Node (E element, Node<E> next) {
			this.element = element;
			this.next = next;
			
		}
		
		//methoden
		
		public E getElement() {
			return element;
		}
		
		public Node<E> getNode(){
			return next;
		}
		
		public void setNext(Node<E> next) {
			this.next = next;
		}
		
	}
}

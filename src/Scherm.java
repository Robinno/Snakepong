import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class Scherm extends JPanel implements Runnable{
	
	private static final long serialVersionUID = 1L;//wilde de compiler dat ik invoerde
	
	private boolean running = false;
	private Thread thread;
	private int aantalGepasseerdeTicks = 0;
	
	private Snake s;
	private Ball b;
	private Paddle p;
	
	private Toetsenbord toetsenbord;
	
	public static final int breedte = 800, hoogte = 400;
	
	public Scherm() {
		//hieronder is om de toetsen te implementeren
		setFocusable(true);
		toetsenbord = new Toetsenbord();
		addKeyListener(toetsenbord);
				
		setPreferredSize(new Dimension(breedte, hoogte));
		
		s = new Snake(this);
		p = new Paddle();
		b = new Ball(this, s, p);
		
		start();
	}
	
	public void start() {
		running = true;
		thread = new Thread(this, "spel");
		thread.start();
	}
	
	public void stop() {
		running = false;
		System.out.println("game over");
		
		//wilde de compiler:
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	

	public void run() {
		
		//dit is de 'loop' van het programma
		while(running) {
			tick(); //updaten
			repaint(); //opnieuw tekenen
		}
	}
	
	public void paint(Graphics g) {
		
		g.clearRect(0, 0, breedte, hoogte);
		
		//ik ga een grid maken, zodat ik goed zie wat er gebeurt
		g.setColor(Color.LIGHT_GRAY);
		for (int i = 0; i<breedte; i++) {
			g.drawLine(i*10,  0,  i*10, hoogte);
		}
		for (int i = 0; i<hoogte; i++) {
			g.drawLine(0, i*10,  breedte ,  i*10);
		}
		//einde maken grid
		
		//componenten tekenen
		s.teken(g);
		b.teken(g);
		p.teken(g);
	}
	
	public void tick() {
		//System.out.println("running ..."); //dit was om te testen of het werkt
		
		
		/*
		 * ik heb het op deze manier opgelost. Ik weet dat er een elegantere manier is, maar
		 * het hele Timer gedoe snapte ik niet echt.
		 */
		
		if (aantalGepasseerdeTicks > 2000000) {
			
			s.tick();
			b.tick();
			p.tick();
			
			aantalGepasseerdeTicks = 0;
		}	
		aantalGepasseerdeTicks++;
		
	}
	
	private class Toetsenbord implements KeyListener{

		@Override
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			
			//hieronder om de pijltjestoetsen in te stellen;
			
			if(key == KeyEvent.VK_RIGHT && !s.getNaarLinks()) {
				s.setNaarBoven(false);
				s.setNaarBeneden(false);
				s.setNaarLinks(false);
				s.setNaarRechts(true);
			}

			if(key == KeyEvent.VK_LEFT && !s.getNaarRechts()) {
				s.setNaarBoven(false);
				s.setNaarBeneden(false);
				s.setNaarLinks(true);
				s.setNaarRechts(false);
			}

			if(key == KeyEvent.VK_UP && !s.getNaarBeneden()) {
				s.setNaarBoven(true);
				s.setNaarBeneden(false);
				s.setNaarLinks(false);
				s.setNaarRechts(false);
			}

			if(key == KeyEvent.VK_DOWN && !s.getNaarBoven()) {
				s.setNaarBoven(false);
				s.setNaarBeneden(true);
				s.setNaarLinks(false);
				s.setNaarRechts(false);
			}
			
			if(key == KeyEvent.VK_Z) {
				p.setNaarBoven(true);
				p.setNaarBeneden(false);
			}
			
			if(key == KeyEvent.VK_S) {
				p.setNaarBoven(false);
				p.setNaarBeneden(true);
			}
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			int key = e.getKeyCode();
			if (key == KeyEvent.VK_Z || key == KeyEvent.VK_S) {
				p.setNaarBoven(false);
				p.setNaarBeneden(false);
			}
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
}

import java.awt.Color;
import java.awt.Graphics;

public class Ball {
	
	private Cell cell;
	private Snake snake;
	private Paddle paddle;
	private Scherm scherm;
	
	private double xvel = .5, yvel = .5; //waarden best kleiner of gelijk aan 1, zodat de snelheid kleiner is dan die van de Snake
	
	public Ball(Scherm scherm, Snake snake, Paddle paddle) {
		cell = new Cell(39,19,10);
		this.snake = snake;
		this.paddle = paddle;
		this.scherm = scherm;
	}
	
	
	public void teken(Graphics g) {
		cell.teken(g, Color.RED);
	}
	
	
	public void tick() {
		
		cell.setX(cell.getX() + xvel);
		cell.setY(cell.getY() + yvel);
		
		//checken voor botsingen met muur
		
		if(cell.getY()<=0||cell.getY()>=39) {
			yvel = -yvel;
		}
		
		//checken voor botsingen met slang
		
		for(int i = 0; i < snake.getSize(); i++) {
			if (cell.getX() == snake.getCell(i).getX() && cell.getY() == snake.getCell(i).getY()){	
				xvel = - Math.abs(xvel);
			}
		}
	
		//checken voor botsingen met paddle
		
		for(int i = 0; i < 5; i++) {
			if (cell.getX() == paddle.getCell(i).getX() && cell.getY() == paddle.getCell(i).getY()){	
				xvel = Math.abs(xvel);
			}
		}
		
		//checken voor buiten veld gaan balleken
		
		if(cell.getX() > 79) {
			scherm.stop();
		}
		
		if(cell.getX() <= 0) {
			cell.setX(39);
			cell.setY(19);
			xvel = 0.5;
			yvel = 0.5;
			snake.eentjeLanger();
		}
	}
	
	
}

import java.awt.Color;
import java.awt.Graphics;

public class Paddle {
	
	//attributen
	
	private Cell[] paddle; //voor de paddle heb ik gekozen om met een array te werken.
	private boolean naarBoven = false, naarBeneden = false;
	
	//constructor
	
	public Paddle() {
		paddle = new Cell[5];
	
		for(int i = 0; i < 5; i ++) {
			paddle[i] = new Cell(4, 19 - 2 + i, 10);
		}
	
	}
	
	//getters en setters
	
	public boolean getNaarBoven() {return naarBoven;}
	public boolean getNaarBeneden() {return naarBeneden;}
	
	public void setNaarBoven(boolean naarBoven) {
		this.naarBoven = naarBoven;
	}

	public void setNaarBeneden(boolean naarBeneden) {
		this.naarBeneden = naarBeneden;
	}

	public Cell getCell(int i) {
		return paddle[i];
	}
	
	//methoden
	
	public void teken(Graphics g) {
		for(int i = 0; i < 5; i++) {
			paddle[i].teken(g, Color.BLUE);
		}
	}
	
	public void tick(){
		
		for(int i = 0; i<5; i++) {
			if(naarBoven) paddle[i].setY(paddle[i].getY() - 1);
			if(naarBeneden) paddle[i].setY(paddle[i].getY() + 1);
		}
		
	}
	
	
}

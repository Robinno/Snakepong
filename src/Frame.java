import java.awt.*;
import javax.swing.JFrame;

public class Frame extends JFrame{

	private static final long serialVersionUID = 1L;//dit wilde de compiler dat ik invoerde

	public Frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Snakepong");
		setResizable(false);
		init();
	}
	
	public void init() {
		setLayout(new GridLayout(1,1,0,0));
		
		Scherm s = new Scherm();
		add(s);
		pack();
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new Frame();
	}
}
